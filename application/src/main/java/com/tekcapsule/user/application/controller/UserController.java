package com.tekcapsule.user.application.controller;

import com.tekcapsule.core.domain.Origin;
import com.tekcapsule.user.application.config.AppConstants;
import com.tekcapsule.user.application.controller.input.*;
import com.tekcapsule.user.application.mapper.InputOutputMapper;
import com.tekcapsule.user.domain.command.*;
import com.tekcapsule.user.domain.model.User;
import com.tekcapsule.user.domain.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class UserController {

    private final UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/users")
    public ResponseEntity<Object> create(@RequestBody CreateInput createInputMessage) {


        log.info(String.format("Entering create user Function - User Id:%s", createInputMessage.getEmailId()));

        Origin origin = Origin.builder().build();

        CreateCommand createCommand = InputOutputMapper.buildCreateCommandFromCreateInput.apply(createInputMessage, origin);
        userService.create(createCommand);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/users/{id}/bookmark")
    public ResponseEntity<Object> addBookmark(@RequestBody AddBookmarkInput addBookmarkInputMessage,@PathVariable String id) {


        log.info(String.format("Entering add bookmark Function - User Id:%S, Capsule Id:%s", addBookmarkInputMessage.getUserId(), addBookmarkInputMessage.getCapsuleId()));

        Origin origin = Origin.builder().build();

        AddBookmarkCommand addBookmarkCommand = InputOutputMapper.buildBookmarkCommandFromBookmarkInput.apply(addBookmarkInputMessage, origin);
        userService.addBookmark(addBookmarkCommand);
        Map<String, Object> responseHeader = new HashMap();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/users/{id}/disable")
    public ResponseEntity<Object> disable(@RequestBody DisableInput disableInputMessage,@PathVariable String id) {


        log.info(String.format("Entering disable user Function - User Id:%s", disableInputMessage.getUserId()));

        Origin origin = Origin.builder().build();

        DisableCommand disableCommand = InputOutputMapper.buildDisableCommandFromDisableInput.apply(disableInputMessage, origin);
        userService.disable(disableCommand);
        Map<String, Object> responseHeader = new HashMap();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/users/{id}/follow")
    public ResponseEntity<Object> follow(@RequestBody FollowTopicInput followTopicInputMessage,@PathVariable String id) {


        log.info(String.format("Entering follow topic Function - User Id:%S, Topic Id:%s", followTopicInputMessage.getUserId(), followTopicInputMessage.getTopicCode()));

        Origin origin = Origin.builder().build();

        FollowTopicCommand followTopicCommand = InputOutputMapper.buildFollowTopicCommandFromFollowTopicInput.apply(followTopicInputMessage, origin);
        userService.followTopic(followTopicCommand);
        Map<String, Object> responseHeader = new HashMap();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> get(@PathVariable String id) {

        log.info(String.format("Entering get user Function -  User Id:%s", id));

        User user = userService.get(id);
        Map<String, Object> responseHeader = new HashMap();
        if (user == null) {
            responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.NOT_FOUND.value());
            user = User.builder().build();
        } else {
            responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/users")
    public ResponseEntity<Object> update(@RequestBody UpdateInput updateInputMessage) {

        log.info(String.format("Entering update user Function - User Id:%s", updateInputMessage.getUserId()));

        Origin origin = Origin.builder().build();

        UpdateCommand updateCommand = InputOutputMapper.buildUpdateCommandFromUpdateInput.apply(updateInputMessage, origin);
        userService.update(updateCommand);
        Map<String, Object> responseHeader = new HashMap();
        responseHeader.put(AppConstants.HTTP_STATUS_CODE_HEADER, HttpStatus.OK.value());

        return new ResponseEntity<>(HttpStatus.OK);
    }
}