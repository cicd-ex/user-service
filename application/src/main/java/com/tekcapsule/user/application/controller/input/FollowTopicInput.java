package com.tekcapsule.user.application.controller.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class FollowTopicInput {
    private String userId;
    private String topicCode;
}
