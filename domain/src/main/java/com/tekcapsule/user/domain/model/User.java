package com.tekcapsule.user.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tekcapsule.core.domain.AggregateRoot;
import com.tekcapsule.core.domain.BaseDomainEntity;
import lombok.*;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseDomainEntity implements AggregateRoot {

    private String userId;
    private Boolean active;
    private String firstName;
    private String lastName;
    private List<String> bookmarks;
    private List<String> subscribedTopics;
    private String emailId;
    private String contactNumber;
    private String activeSince;
}