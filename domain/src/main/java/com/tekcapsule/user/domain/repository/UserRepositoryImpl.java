package com.tekcapsule.user.domain.repository;

import com.tekcapsule.user.domain.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository
public class UserRepositoryImpl implements UserDynamoRepository {


    @Autowired
    public UserRepositoryImpl() {
    }

    @Override
    public List<User> findAll() {

        return null;
    }

    @Override
    public User findBy(String userId) {
        return null;
    }

    @Override
    public User save(User user) {
        return user;
    }

}
